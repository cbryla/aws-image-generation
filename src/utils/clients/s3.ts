import { S3 } from 'aws-sdk';
import { logger } from '@utils/index';

const convertPath = (path: string) => {
  return path.replace(/\/?$/, '/');
};

class S3Client {
  private readonly bucketName: string;
  private s3Bucket: S3;
  public constructor(bucketName: string) {
    this.bucketName = bucketName;
    this.s3Bucket = new S3();
  }

  public async upload(content: string, fileName: string, path: string = '') {
    if (path !== '') {
      //ensure ends with /
      path = convertPath(path);
    }
    await this.s3Bucket
      .putObject({
        Bucket: this.bucketName,
        Key: path + fileName + '.svg',
        Body: content,
        ContentType: 'image/svg',
      })
      .promise();
  }

  public async getFile(fileName: string, path: string = '') {
    if (path !== '') {
      path = convertPath(path);
    }

    try {
      const file = await this.s3Bucket
        .getObject({
          Bucket: this.bucketName,
          Key: path + fileName + '.svg',
        })
        .promise();

      return file.Body.toString('utf-8');
    } catch (e) {
      logger.info(fileName + ' does not exist in s3 bucket.');
      logger.error(e);
      return null;
    }
  }
}

export default S3Client;
