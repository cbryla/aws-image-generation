// @ts-ignore
import * as D3Node from 'd3-node';
import { ScaleBand, scaleBand } from 'd3';
import {
  margin,
  svgWidth,
  svgHeight,
  width,
  height,
  topLabelHeight,
  speciesStart,
  speciesAxisWidth,
  tickOffset,
  colors,
  whiteLetters,
  animalSpeciesAliases
} from './constants';
import AlignmentDataManager from './AlignmentDataManager';
import {
  ImageType,
  NUCLEOTIDE_EXTENDED_ALIGNMENT,
  PROTEIN_EXTENDED_ALIGNMENT
} from './constants/ImageTypes';
import STYLES from './constants/styles';

export default class BaseAlignment {
  protected data: {
    height: number;
    svgHeight: number;
    speciesStart: number;
  };
  protected defs: D3Node;
  protected _cells: { [key: string]: string } = {};
  protected dataManager: AlignmentDataManager;
  protected imageType: ImageType;
  protected colors: {
    [key: string]: string
  };
  protected d3n: D3Node;
  protected svg: D3Node;
  protected scaleX: ScaleBand<string | number | number[]>;
  protected scaleY: ScaleBand<string | number | number[]>;
  protected whiteLetters: string[];


  constructor(alteration: {}) {
    this.data = Object.assign(alteration, { height, svgHeight, speciesStart });
  }

  render() {
    this.setImageType().initDataManager();
    // Not all alterations will have a protein alignment
    if (!this.dataManager.canRender()) {
      return false;
    }

    return this.initD3n()
      .initSvg()
      .evoArrow()
      .scales()
      .evoTree()
      .animalClassLabels()
      .ticks()
      .speciesLabels()
      .alignments()
      .extendedAlignment()
      .variantConservationHighlight()
      .subTitle()
      .svgString();
  }

  setImageType() {
    this.imageType = null;
    return this;
  }

  initDataManager() {
    this.dataManager = new AlignmentDataManager(this.data, this.imageType);
    const imgKey = this.imageType.toLowerCase().replace(/^(\w+).*/, '$1');
    this.colors = colors[imgKey];
    this.whiteLetters = whiteLetters[imgKey];

    return this;
  }

  initD3n() {
    this.d3n = new D3Node({ STYLES });
    return this;
  }

  initSvg() {
    this.svg = this.d3n
      .createSVG(svgWidth / 2, this.data.svgHeight / 2)
      .attr('xmlns:xlink', 'http://www.w3.org/1999/xlink')
      .attr('viewBox', `0 0 ${svgWidth} ${this.data.svgHeight}`)
      .append('g')
      .attr('id', 'nucleotide-image')
      .attr('transform', `translate(${margin},${margin})`);

    this.defs = this.svg.append('defs');

    return this;
  }

  isExpanded() {
    return this.imageType === NUCLEOTIDE_EXTENDED_ALIGNMENT || this.imageType === PROTEIN_EXTENDED_ALIGNMENT;
  }

  variantConservationHighlight() {
    if (this.dataManager.variantLength <= 50) {
      const {
        svg,
        scaleX,
        data: { height },
        dataManager: { variantStart, variantLength }
      } = this;

      svg
        .append('rect')
        .attr('x', scaleX(variantStart))
        .attr('y', tickOffset + 10)
        .attr('width', variantLength * scaleX.step())
        .attr('height', height - tickOffset - 10)
        .attr('stroke', 'black')
        .attr('stroke-width', 4)
        .attr('fill', 'none');
    }

    return this;
  }

  cellId(cellLetter: string) {
    const ref = cellLetter.toLowerCase();
    const isLowercase = cellLetter === cellLetter.toLowerCase();
    const refId = `${ref}-cell-${isLowercase ? 'lc' : 'uc'}`;
    if (!this._cells[cellLetter]) {
      const { scaleX, scaleY } = this;
      const textOffsets: { [key: string]: number } = { g: 6 };

      const cellGroup = this.defs.append('g').attr('id', refId);

      cellGroup
        .append('rect')
        .attr('width', scaleX.bandwidth())
        .attr('height', scaleY.bandwidth())
        .attr('fill', () => this.colors[ref] || '#808080')
        .attr('stroke-width', 2)
        .attr('stroke', '#CDCDCD');

      cellGroup
        .append('text')
        .attr('x', () => scaleX.bandwidth() / 2)
        .attr('y', () => scaleY.bandwidth() / 2 + (textOffsets[cellLetter] || 7))
        .attr('fill', () => (this.whiteLetters.indexOf(ref) === -1 ? '#000' : '#FFF'))
        .attr('text-anchor', 'middle')
        .text(cellLetter);

      return (this._cells[cellLetter] = refId);
    }

    return refId;
  }

  alignments() {
    const {
      scaleX,
      scaleY,
      dataManager: { alignmentList }
    } = this;

    this.svg
      .append('g')
      .attr('transform', `translate(0, 0)`)
      .selectAll('g')
      .data(alignmentList)
      .enter()
      .append('g')
      .attr('transform', (_: unknown, i: number) => `translate(0,${scaleY(i)})`)
      .selectAll('use')
      .data((d: string) => d.split(''))
      .enter()
      .append('use')
      .attr('x', (_: unknown, i: number) => scaleX(i))
      .attr('xlink:xlink:href', (d: string) => `#${this.cellId(d)}`);

    return this;
  }

  speciesLabels() {
    const {
      svg,
      scaleY,
      colorScale,
      data: { speciesStart },
      dataManager: { speciesList }
    } = this;

    const speciesG = svg.append('g').attr('transform', `translate(${speciesStart},0)`);

    speciesG
      .selectAll('text.species')
      .data(speciesList)
      .enter()
      .append('text')
      .attr('y', (_: unknown, i: number) => scaleY(i) + 15)
      .attr('fill', () => colorScale())
      .text((d: string) => animalSpeciesAliases[d] || d);

    return this;
  }

  scales() {
    this.scaleX = scaleBand()
      .domain(new Array(51).fill(0).map((_: unknown, i: number) => i.toString()))
      .range([speciesAxisWidth + (this.isExpanded() ? 20 : 0), width - 200]);

    this.scaleY = scaleBand()
      .domain(new Array(this.dataManager.speciesList.length).fill(0).map((_: unknown, i: number) => i.toString()))
      .range([topLabelHeight, this.data.height]);

    return this;
  }

  evoArrow() {
    const {
      svg,
      data: { height }
    } = this;

    const gradient = this.defs
      .append('linearGradient')
      .attr('id', 'linear')
      .attr('x1', '0%')
      .attr('x2', '0%')
      .attr('y2', '0%')
      .attr('y1', '100%');

    gradient
      .append('stop')
      .attr('offset', '0%')
      .attr('stop-color', '#6792C1');
    gradient
      .append('stop')
      .attr('offset', '80%')
      .attr('stop-color', '#E9E9E9');

    svg
      .append('marker')
      .attr('id', 'arrow')
      .attr('viewBox', '0 0 10 10')
      .attr('refX', 0)
      .attr('refY', 5)
      .attr('markerUnits', 'strokeWidth')
      .attr('markerWidth', 4)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('path')
      .attr('d', 'M 0 0 L 10 5 L 0 10 z')
      .attr('fill', '#E9E9E9')
      .attr('stroke', 'black')
      .attr('stroke-width', 0.3);

    svg
      .append('line')
      .attr('marker-end', 'url(#arrow)')
      .attr('stroke', '#E9E9E9')
      .attr('stroke-width', 15)
      .attr('x1', 30)
      .attr('x2', 30)
      .attr('y2', 80)
      .attr('y1', 90);

    svg
      .append('rect')
      .attr('x', 23)
      .attr('height', height - 75)
      .attr('width', 15)
      .attr('y', 80)
      .attr('fill', 'url(#linear)')
      .attr('stroke', 'black');

    return this;
  }

  svgString() {
    return this.d3n.svgString();
  }

  animalClassLabels() {
    // const keyedSpecies = this.data.animalClasses.reduce((obj, animal) => {
    //   obj[animal.start]
    // }, {});
    // const labels = this.dataManager.speciesList.reduce((labels, species, index, arr) => {}, []);
    const labelGs = this.svg
      .append('g')
      .attr('transform', 'translate(80,0)')
      .selectAll('g.labels')
      .data(this.dataManager.animalClasses)
      .enter()
      .append('g')
      .attr('class', 'labels')
      .attr('transform', ((d: D3Node) => {
        if (d.transformY) {
          return `translate(0,${d.transformY})`;
        }
        const index = this.dataManager.speciesList.findIndex(species => species.toLowerCase() === d.start);
        return `translate(0,${this.scaleY(index) + this.scaleY.step()})`;
      }));

    labelGs
      .append('text')
      .attr('y', -2)
      .text(({ s }: { s: string }) => s);

    labelGs
      .append('text')
      .attr('y', 20)
      .text(({ l }: { l: string }) => l);

    labelGs
      .append('line')
      .attr('x2', ({ x2 }: { x2: string }) => x2)
      .attr('stroke', '#5283bb');

    return this;
  }

  evoTree() {
    const treeAlignmentG = this.svg.append('g').attr('transform', `translate(200,0)`);
    const { evoTreeSettings } = this.dataManager;
    treeAlignmentG
      .append('path')
      .attr('d', evoTreeSettings.d)
      .attr('transform', evoTreeSettings.transform);

    return this;
  }

  ticks() {
    const {
      scaleX,
      svg,
      dataManager: { labels, variantTick }
    } = this;

    const alignment = svg.append('g').attr('transform', 'translate(200,0)');

    const tickG = alignment
      .selectAll('g')
      .data([0, 10, 20, variantTick, 30, 40, 50])
      .enter()
      .append('g')
      .attr('transform', (d: number) => `translate(${scaleX(d)},${d === variantTick ? tickOffset - 2 : tickOffset})`);

    tickG
      .append('line')
      .attr('x1', 10)
      .attr('x2', 10)
      .attr('y1', (d: number) => (d === variantTick ? -5 : 0))
      .attr('y2', 10)
      .attr('stroke', 'black')
      .attr('stroke-width', 3);

    tickG
      .append('text')
      .attr('y', (d: number) => (d === variantTick ? -10 : -5))
      .attr('x', 10)
      .attr('text-anchor', 'middle')
      .text((d: number) => labels[d]);

    this.svg = alignment;

    return this;
  }

  subTitle() {
    const {
      dataManager,
      dataManager: { alignmentList, replacement },
      scaleY,
      scaleX
    } = this;
    const lastRow = this.scaleY(alignmentList.length - 1);
    this.svg
      .append('text')
      .attr('y', lastRow + scaleY.step() * 1.75)
      .attr('x', scaleX(0))
      .text('Danio rerio*: common name "Zebrafish"');

    if (dataManager.isInsertion() && replacement.length > 1) {
      const insertionGroup = this.svg.append('g').attr('transform', `translate(0,${lastRow + scaleY.step() * 3})`);
      const insertionLabel = '+(inserted string):';
      insertionGroup
        .append('text')
        .attr('x', -150)
        .text(insertionLabel);
      const textOffset = insertionLabel.length * 2;
      insertionGroup
        .selectAll('use')
        .data(() => replacement.split(''))
        .enter()
        .append('use')
        .attr('x', (_: unknown, i: number) => scaleX.step() * i + textOffset)
        .attr('y', scaleY.step() * -0.8)
        .attr('xlink:xlink:href', (d: string) => `#${this.cellId(d)}`);
    }

    return this;
  }

  insertedLegend() {
    this.svg.append('g');
  }

  extendedAlignment() {
    return this;
  }

  colorScale() {
    return '#000';
  }
};
