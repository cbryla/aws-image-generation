import BaseAlignment from './BaseAlignment';
import { NUCLEOTIDE_ALIGNMENT } from './constants/ImageTypes';

export default class NucleotideAlignment extends BaseAlignment {
    setImageType() {
      this.imageType = NUCLEOTIDE_ALIGNMENT;
      return this;
    }
  };