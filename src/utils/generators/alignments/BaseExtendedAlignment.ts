import BaseAlignment from './BaseAlignment';
import {
  height,
  svgHeight,
  speciesStart
} from './constants/constants-extended';
import extended_tree_base64 from './img/extended_tree_base64';

export default class BaseExtendedAlignment extends BaseAlignment {
  constructor(alteration: {}) {
    super(alteration);
    this.data = Object.assign(this.data, { height, svgHeight, speciesStart });
  }

  evoTree() {
    const { scaleY } = this;
    const step = scaleY.step();
    const treeAlignmentG = this.svg.append('g').attr('transform', `translate(175,${scaleY(1)})`);
    const [top, bottom] = scaleY.range();

    const alignmentHeight = bottom - top - step;

    treeAlignmentG
      .append('image')
      .attr('xlink:href', extended_tree_base64)
      .attr('transform', `scale(${alignmentHeight / 1602})`);

    return this;
  }

  speciesClassLines() {
    const {
      scaleX,
      scaleY,
      dataManager: { speciesList }
    } = this;
    const [x1, x2] = scaleX.range();
    const breakPoints = this.dataManager.animalClasses.map((animalClass: { start: string }) =>
      speciesList.findIndex(species => species.toLowerCase() === animalClass.start)
    );

    this.svg
      .selectAll('line.species-guide')
      .data(breakPoints)
      .enter()
      .append('line')
      .attr('class', 'species-guide')
      .attr('y1', scaleY)
      .attr('y2', scaleY)
      .attr('x1', x1 + 1)
      .attr('x2', x2 + 1)
      .attr('stroke', 'black')
      .attr('stroke-width', 2);

    return this;
  }

  extendedAlignment() {
    this.speciesClassLines();
    return this;
  }
};
