import BaseAlignment from './BaseAlignment';
import { PROTEIN_ALIGNMENT } from './constants/ImageTypes';

export default class ProteinAlignment extends BaseAlignment {
  setImageType() {
    this.imageType = PROTEIN_ALIGNMENT;
    return this;
  }
};
