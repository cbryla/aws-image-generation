const bottomOffset = 40;
const rightOffset = 10;
const globalColors = {
  '-': '#808080',
  '+': '#808080',
  a: '#CC6666',
  p: '#CC6666',
  w: '#CC6666',
  f: '#CC6666',
  l: '#CC6666',
  i: '#CC6666',
  v: '#CC6666',
  m: '#CC6666',

  e: '#6666CC',
  d: '#6666CC',

  t: '#66CC66',
  y: '#66CC66',
  h: '#66CC66',
  s: '#66CC66',
  n: '#66CC66',
  q: '#66CC66',

  k: '#CC66CC',
  r: '#CC66CC'
};

export const svgWidth = 1670 + rightOffset;
export const svgHeight = 1160 + bottomOffset;
export const margin = 20;
export const speciesStart = 210;
export const fontSize = 20;
export const width = svgWidth - rightOffset - margin * 2;
export const height = svgHeight - bottomOffset - margin * 2;
export const topLabelHeight = 30;
export const speciesAxisWidth = speciesStart + 155;
export const tickOffset = 20;
export const colors: { [key: string]: {} } = {
  protein: Object.assign(
    {
      g: '#66CC66',
      c: '#66CC66'
    },
    globalColors
  ),
  nucleotide: Object.assign(
    {
      c: '#6666CC',
      g: '#CC66CC'
    },
    globalColors
  )
};
export const whiteLetters: { [key: string]: string[] } = { protein: ['d', 'e', 'k', 'r'], nucleotide: ['c', 'g'] };
export const animalSpeciesAliases: { [key: string]: string } = {
  'Xenopus tropicalis': 'X. tropicalis'
};
export const animalClasses: { [key: string]: {}[] } = {
  V2: [
    { s: 'Humans', l: '4.7 million years ago', y: 75, x2: 297, start: 'human' },
    { transformY: 260, s: 'Primates', l: '54-65 MYA', y: 260, x2: 225, start: 'marmoset' },
    { transformY: 872, s: 'Mammals', l: '213 MYA', y: 875, x2: 140, start: 'platypus' },
    { transformY: 924, s: 'Reptiles', l: '408 MYA', y: 925, x2: 130, start: 'zebra finch' },
    { transformY: 1053, s: 'Fish', l: '530 MYA', y: 1055, x2: 120, start: 'medaka' }
  ],
  V3: [
    { s: 'Humans', l: '4.7 million years ago', y: 75, x2: 297, start: 'human' },
    { transformY: 240, s: 'Primates', l: '54-65 MYA', y: 260, x2: 225, start: 'marmoset' },
    { transformY: 862, s: 'Mammals', l: '213 MYA', y: 875, x2: 141, start: 'platypus' },
    { transformY: 913, s: 'Reptiles', l: '408 MYA', y: 925, x2: 130, start: 'chicken' },
    { s: 'Fish', l: '530 MYA', y: 1055, x2: 120, start: 'atlantic cod' }
  ]
};
export const latinNames = ['African clawed frog', 'Zebrafish'];
