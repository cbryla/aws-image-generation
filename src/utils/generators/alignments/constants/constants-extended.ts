const bottomOffset = 40;
const margin = 20;

export const svgHeight = 2320 + bottomOffset;
export const height = svgHeight - bottomOffset - margin * 2;
export const speciesStart = 145;
export const animalClasses = [
    { s: 'Humans', l: '4.7 million years ago', y: 49, x2: 120, start: 'human' },
    { s: 'Primates', l: '54-65 MYA', y: 274, x2: 120, start: 'chinese tree shrew' },
    { s: 'Mammals', l: '213 MYA', y: 1209, x2: 120, start: 'saker falcon' },
    { s: 'Reptiles', l: '408 MYA', y: 1469, x2: 120, start: 'american alligator' },
    { s: 'Fish', l: '530 MYA', y: 1604, x2: 110, start: 'coelacanth' }
];
export const animalClassColors = ['#000', '#F2671F', '#C91B26', '#9C0F5F', '#60047A', '#160A47'];
export const speciedBreakPoints = ['human', 'chinese tree shrew', 'saker falcon', 'american alligator', 'coelacanth'];
