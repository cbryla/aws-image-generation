import BaseExtendedAlignment from './BaseExtendedAlignment';
import { NUCLEOTIDE_EXTENDED_ALIGNMENT } from './constants/ImageTypes';

export default class NucleotideExtendedAlignment extends BaseExtendedAlignment {
  setImageType() {
    this.imageType = NUCLEOTIDE_EXTENDED_ALIGNMENT;
    return this;
  }
};