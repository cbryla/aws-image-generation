// @ts-ignore
import * as D3Node from 'd3-node';

export const createNaSvg = (error = ''): string => {
  const err = error.trim().replace(/"/, "'");
  return `<svg xmlns='http://www.w3.org/2000/svg' height='20' width='40'>
    <text data-error='${err}' x='0' y='16' font-family='Verdana' font-size='16'>N/A</text>
  </svg>`;
};

export const scaleSvg = (content: string, scale: number) => {
  const d3n = new D3Node();
  d3n.d3Element.html(content);
  const svg = d3n.d3Element.select('svg');
  const width = svg.attr('width');
  const height = svg.attr('height');
  const viewBox = svg.attr('viewBox');
  // If we don't have any of these properties then we can't scale anything
  if (!scale || (!width && !height && !viewBox)) {
    return content;
  }
  // If there is a height and width then we set the viewbox and scale height and with
  if (height || width) {
    if (width) {
      svg.attr('width', width * scale);
    }

    if (height) {
      svg.attr('height', height * scale);
    }

    // If there is no viewbox we can set it to account for scale
    if (!viewBox) {
      svg.attr('viewBox', `0 0 ${width} ${height}`);
    }
  } else if (viewBox) {
    // There is no height/width but there is a viewbox
    const [vWidth, vHeight] = viewBox.split(/,\s?|\s/);
    // When we create a height and with based on the viewbox as teh base
    svg.attr('height', vHeight * scale).attr('width', vWidth * scale);
  }

  return d3n.svgString();
};