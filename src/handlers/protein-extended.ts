import AbstractHandler from './AbstractHandler';
import ProteinExtendedAlignment from '@utils/generators/alignments/ProteinExtendedAlignment';

const FOLDER = 'protein-extended';

export const handler = new AbstractHandler(ProteinExtendedAlignment, FOLDER).createHandler();