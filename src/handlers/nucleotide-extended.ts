import AbstractHandler from './AbstractHandler';
import NucleotideExtendedAlignment from '@utils/generators/alignments/NucleotideExtendedAlignment';

const FOLDER = 'nucleotide-extended';

export const handler = new AbstractHandler(NucleotideExtendedAlignment, FOLDER).createHandler();
