import AbstractHandler from './AbstractHandler';
import NucleotideAlignment from '@utils/generators/alignments/NucleotideAlignment';

const FOLDER = 'nucleotide';

export const handler = new AbstractHandler(NucleotideAlignment, FOLDER).createHandler();